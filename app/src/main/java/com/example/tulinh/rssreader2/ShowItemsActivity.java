package com.example.tulinh.rssreader2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ShowItemsActivity extends AppCompatActivity implements UserObserver {

    Feed currentFeed;
    ListView itemListView;
    ArrayAdapter<Item> itemArrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_items);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        // Get intent and feed object
        Intent intent = getIntent();
        currentFeed = (Feed)intent.getSerializableExtra("Feed object");

        // Register this activity as a user observer
        User.getInstance().addObserver(this);
        itemListView = (ListView) findViewById(R.id.item_listView);

        // start parsing to get items for the currentFeed
        try {
            XMLHandler handler = new XMLHandler(currentFeed);
            Thread thread = new Thread(handler);
            thread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

        clickOnItem();
    }


    public void clickOnItem() {
        itemListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
                //Create new instances of Intent and Bundle
                Intent intent = new Intent(ShowItemsActivity.this,ShowItemDescriptionActivity.class);
                Bundle bundle = new Bundle();

                //Put object Item to intent to pass to ShowItemDescriptionActivity
                bundle.putSerializable("Item",currentFeed.getItems().get(position));
                intent.putExtras(bundle);

                //Start ShowItemDescriptionActivity
                startActivity(intent);
            }
        });
    }


    @Override
    public void update() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Set up adapter
                    itemArrayAdapter = new ArrayAdapter<>(ShowItemsActivity.this,R.layout.item_view,currentFeed.getItems());

                    // Configure listView
                    itemListView.setAdapter(itemArrayAdapter);
                    itemArrayAdapter.notifyDataSetChanged();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.show_items_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()){
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
