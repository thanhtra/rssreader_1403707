package com.example.tulinh.rssreader2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    User user = User.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        renderFeedList();
        clickOnFeed();

    }

    public void renderFeedList() {
        try {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //List of feeds
                    List<Feed> feeds = user.getFeeds();

                    // Build adapter
                    ArrayAdapter<Feed> adapter = new ArrayAdapter<>(MainActivity.this,R.layout.feed_view,feeds);

                    //Configure ListView
                    ListView listView = (ListView) findViewById(R.id.feed_listView);
                    listView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clickOnFeed() {
        ListView listView = (ListView) findViewById(R.id.feed_listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View viewClicked, int position, long id) {
                //Create new instances of Intent and Bundle
                Intent intent = new Intent(MainActivity.this,ShowItemsActivity.class);
                Bundle bundle = new Bundle();

                //Put object feed to intent to pass to ShowItemsActivity
                bundle.putSerializable("Feed object",user.getFeeds().get(position));
                intent.putExtras(bundle);

                // Start ShowItemsActivity
                startActivity(intent);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()){
            case (R.id.action_add):
                openAdd();
                return true;
            case (R.id.action_settings):
                openSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openAdd() {

    }

    public void openSettings() {

    }
}
