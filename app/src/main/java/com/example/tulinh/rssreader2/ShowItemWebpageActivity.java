package com.example.tulinh.rssreader2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class ShowItemWebpageActivity extends AppCompatActivity {

    private String urlString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_item_webpage);

        Intent intent = getIntent();
        urlString = intent.getStringExtra("url string");

        WebView webView = (WebView) findViewById(R.id.item_webView);
        webView.loadUrl(urlString);


    }
}
